# Using Ddev

## Local installation

Requirements:
* [Ddev](https://ddev.com/)


## Steps first installation of WinterCMS:
1. Clone this repository locally
2. From the project folder add your SSH keys to the DDev container with the SSH agent `ddev auth ssh`

## WinterCMS version
Latest WinterCMS development version require PHP 8.1
This scaffold is set to PHP 8.1\

Use: `ddev composer create -y wintercms/winter` for the latest stable version

## Install WinterCMS
4. Install WINTERCMS with `ddev build`
5. Launch the stack `ddev start`

## Backend user
* Username: admin
* Pass: admin